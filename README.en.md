# Wechat

#### Description
利用fragment实现wechat的基本界面

# mobilePhone

## 初始界面
![img_1.png](img_1.png)

#### 界面一
![img_2.png](img_2.png)

#### 界面二
![img_3.png](img_3.png)

#### 界面三
![img_4.png](img_4.png)

#### 界面四
![img_5.png](img_5.png)



#### 本次主要微信界面的搭建，但是代码存在冗余的部分，有优化的空间
