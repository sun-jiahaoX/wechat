package com.example.weichat_swu_sjh

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class BottomView: Fragment(), View.OnClickListener  {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bottom_view,container,false)
    }
    //设置监听
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.btn_1).setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_2).setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_3).setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_4).setOnClickListener(this)
    }
    //点击事件
    override fun onClick(v: View?) {
        val act = requireActivity() as MainActivity
        if (v != null) {
            when (v.id) {
                R.id.btn_1 -> {
                    act.showFirst()
                }
                R.id.btn_2 -> {
                    act.showSecond()
                }
                R.id.btn_3 -> {
                    act.showThrid()
                }
                R.id.btn_4 -> {
                    act.showFourth()
                }
            }
        }
    }
}