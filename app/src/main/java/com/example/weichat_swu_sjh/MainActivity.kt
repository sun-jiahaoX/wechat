package com.example.weichat_swu_sjh

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }
//代码冗余过高，应该像个办法写进一个函数里面的
    fun showFirst() {
        val fragmentManager =supportFragmentManager
        val transaction =fragmentManager.beginTransaction()
        transaction.replace(R.id.topFrag,FirstView())
        transaction.commit()
    }

    fun showSecond() {
        val fragmentManager =supportFragmentManager
        val transaction =fragmentManager.beginTransaction()
        transaction.replace(R.id.topFrag,SecondView())
        transaction.commit()
    }

    fun showThrid() {
        val fragmentManager =supportFragmentManager
        val transaction =fragmentManager.beginTransaction()
        transaction.replace(R.id.topFrag,ThirdView())
        transaction.commit()
    }

    fun showFourth() {
        val fragmentManager =supportFragmentManager
        val transaction =fragmentManager.beginTransaction()
        transaction.replace(R.id.topFrag,FourthView())
        transaction.commit()
    }
}
